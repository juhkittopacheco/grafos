 // Criando um objeto com todas as funções que serão usadas
var transformar = {
	init: function() {//ao iniciar chama todas as funções que estão aqui
		this.events();
	},
	events: function() {//todos os eventos como click, e outros

		// faz referência a objeto transformar
		var self_ = this;

		// captura o clique no botão 'CRIAR'
		$('body').on('click','.section--criar', function() {
			// junta todas as informações dadas pelo usuário
			var novoGrafo = {
				tipo: $('.tipo').val(),
				quantidade_nos: $('.qtd_nos').val(),
				e_valorado: $('.e_valorado').val()
			}	

			if((novoGrafo.tipo == 'lista-matriz')&&(novoGrafo.e_valorado == 'nao'))
				self_.criarLista(novoGrafo);

			if((novoGrafo.tipo == 'lista-matriz')&&(novoGrafo.e_valorado == 'sim'))
				self_.criarListaValorada(novoGrafo);

			if((novoGrafo.tipo == 'matriz-lista'))
				self_.criarMatriz(novoGrafo, novoGrafo.e_valorado);

		})

		// captura o clique no botão 'TRANSFORMAR'
		$('body').on('click','.section--transformar', function() {

			if(!self_.valida('todos'))
				return false;
			
			if($(this).attr('data-tipo') == 'lista-matriz-nao'){			
				self_.transformarListaMatrizNao($(this).parents('.section--2').find('li'), $('.qtd_nos').val());
			}

			if($(this).attr('data-tipo') == 'lista-matriz-sim'){
				self_.transformarListaMatrizSim($(this).parents('.section--2').find('li'), $('.qtd_nos').val());
			}


			if($(this).attr('data-tipo') == 'matriz-lista-nao'){
				self_.transformarMatrizListaNao($(this).parents('.section--2').find('table'), $('.qtd_nos').val());
			}

			if($(this).attr('data-tipo') == 'matriz-lista-sim'){
				self_.transformarMatrizListaSim($(this).parents('.section--2').find('table'), $('.qtd_nos').val());
			}

		})
	},
	escondeQuestionario: function(){ //esconde o questionario inicial
		$('.section--1').fadeOut();
	},
	criarLista: function(novoGrafo) {//cria lista com campos para o usuário preencher
		this.escondeQuestionario();
		// faz referência a objeto transformar
		var alfabeto = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

		// Criando o Html da lista com os campos selecionado pelo usuário
		var htmlLista =  `
			<div class="section--2">
				<div class="section--title">
					<h2>Preencha os campos com os vértices adjacentes.</h2>
					<p>exemplo: A: A, B, C, E</p>
				</div>

				<ul class="lista">
		`;

		// looping que cria campos em frente ao nós para que o usuário digite a adjacência7
		for(var i = 0; i < parseInt(novoGrafo.quantidade_nos); i++){
			htmlLista += `
				<li>
					<label>`+alfabeto[i]+`:</label>
					<input type="text" class="verticeAdjacente-`+alfabeto[i]+`" value=""/>	
				</li>`;
		}
					

		htmlLista += 
				`</ul>
				<div class="section--linha">
					<div class="section--btn section--transformar" data-tipo="lista-matriz-`+novoGrafo.e_valorado+`">TRANSFORMAR</div>
				</div>
			</div>`;
		

		$('.shell').append(htmlLista);

	},
	criarMatriz: function(novoGrafo, tipo) {//cria lista com campos para o usuário preencher
		this.escondeQuestionario();
		var alfabeto = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
		// inicializa table
		var htmlMatriz = `<div class="newMatriz section--2">
				<div class="section--title">
					<h2>Preencha os campos com os vértices adjacentes.</h2>`;

		htmlMatriz += tipo == 'nao'?
				`<p>Digite 1 para os vértices adjacentes e 0 para os que não são</p>`:
				`<p>Digite os valores dentro vértices adjacentes e 0 para os que não são</p>`;


		htmlMatriz += `</div><table>`;
		
		htmlMatriz += `<th></th>`;	

		// coloca o cabeçalho
		for(var i = 0; i < parseInt(novoGrafo.quantidade_nos); i++){//percorreo cada vertice mesmo que não seja adjacente
			htmlMatriz += `<th>`+alfabeto[i]+`</th>`;	
		}

		for(var i_ = 0; i_ < parseInt(novoGrafo.quantidade_nos); i_++) {//percorre cada vertice

			htmlMatriz += `<tr>
			<td>`+alfabeto[i_]+`</td>`;
			for(var j = 0; j < parseInt(novoGrafo.quantidade_nos); j++){//percorreo cada vertice mesmo que não seja adjacente
				htmlMatriz += `<td><input type="text" class="verticeAdjacente-`+alfabeto[j]+`" value=""/></td>`;
			}	
			htmlMatriz += `<tr>`;

		};


		htmlMatriz += `</table>
				<div class="section--linha">
					<div class="section--btn section--transformar" data-tipo="matriz-lista-`+novoGrafo.e_valorado+`">TRANSFORMAR</div>
				</div>
			</div>`;
		

		$('.shell').append(htmlMatriz);

	},
	criarListaValorada: function(novoGrafo) {//cria lista com campos para o usuário preencher
		this.escondeQuestionario();
		// faz referência a objeto transformar
		var alfabeto = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

		// Criando o Html da lista com os campos selecionado pelo usuário
		var htmlLista =  `
			<div class="section--2">
				<div class="section--title">
					<h2>Preencha os campos com os vértices adjacentes.</h2>
					<p>exemplo: A: (2,A) ; (3,B) ; (4,C) ; (5,D) </p>
				</div>

				<ul class="lista">
		`;

		// looping que cria campos em frente ao nós para que o usuário digite a adjacência7
		for(var i = 0; i < parseInt(novoGrafo.quantidade_nos); i++){
			htmlLista += `
				<li>
					<label>`+alfabeto[i]+`:</label>
					<input type="text" class="verticeAdjacente-`+alfabeto[i]+`" value=""/>	
				</li>`;
		}
					

		htmlLista += 
				`</ul>
				<div class="section--linha">
					<div class="section--btn section--transformar" data-tipo="lista-matriz-`+novoGrafo.e_valorado+`">TRANSFORMAR</div>
				</div>
			</div>`;
		

		$('.shell').append(htmlLista);
	},
	transformarListaMatrizNao: function(lista, quantidade_nos) {//transforma lista em matriz não valorado
		var vetorLista = [];

		// percorre a lista em html recebida
		lista.each(function(index, value) {
			// captura os elementos que estão dentro do input de cada vertice e separa eles dentro de um vetor
			vetorLista[index] = $(this).find('input').val().toLowerCase().split(',');
			
			// transformar as letras da lista em numero para que fique mais facil fazer a conversão
			vetorLista[index].forEach(function(value, index_) {
				vetorLista[index][index_] = value.charCodeAt(0) - 97;
			});

		});

		this.criarMatrizComLista(vetorLista, quantidade_nos,'nao');

	},
	transformarListaMatrizSim: function(lista, quantidade_nos) {//transforma lista em matriz valorado
		var vetorLista = [];

		// percorre a lista em html recebida
		lista.each(function(index, value) {
			// captura os elementos que estão dentro do input de cada vertice e separa eles dentro de um vetor
			vetorLista[index] = $(this).find('input').val().toLowerCase().replace(/\(/g,'').replace(/\)/g,'').split(';');


			vetorLista[index].forEach(function(_value,_index) {
				try {
					vetorLista[index][_index] = _value.split(',');
					vetorLista[index][_index][1] = vetorLista[index][_index][1].replace(' ','').charCodeAt(0) - 97;
				}
				catch(error){
					alert('Preencha os campos corretamente');
					return false;
				}
				
			})		

		});

		this.criarMatrizComLista(vetorLista, quantidade_nos,'sim');

	},
	transformarMatrizListaNao: function(lista, quantidade_nos) {//transforma Matriz em lista não valorada
		var vetorMatriz = [], newVetorMatriz = [];//zera algumas variáveis
		// percorre a lista em html recebida
		lista.find('tr:not(:first-child):not(:empty)').each(function(index, value) {//perco9rre todas as colunas
			newVetorMatriz = [];//zera variável
			$(this).find('td:not(:first-child)').each(function(_index, _value) {//percorre todas as celulas

				// captura os elementos que estão dentro do input de cada vertice
				newVetorMatriz[_index] = $(this).find('input').val().toLowerCase();
				
			});

			vetorMatriz[index] = newVetorMatriz;


		});
		this.criarListaComMatriz(vetorMatriz, quantidade_nos,'nao');

	},
	transformarMatrizListaSim: function(lista, quantidade_nos) {//transforma matriz em lista não valorada
		var vetorMatriz = [], newVetorMatriz = [];//zera variáveis
		// percorre a lista em html recebida
		lista.find('tr:not(:first-child):not(:empty)').each(function(index, value) {//percorre doas as colunas
			newVetorMatriz = [];
			$(this).find('td:not(:first-child)').each(function(_index, _value) {//percorre todas as celulas

				// captura os elementos que estão dentro do input de cada vertice
				newVetorMatriz[_index] = $(this).find('input').val().toLowerCase();
				
			});

			vetorMatriz[index] = newVetorMatriz;


		});
		this.criarListaComMatriz(vetorMatriz, quantidade_nos,'sim');

	},
	criarMatrizComLista: function(vetor, quantidade_nos, tipo) {//cria matriz e preenche ela com conteúdo da tradução
		var alfabeto = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
		// inicializa table
		var htmlMatriz = `<div class="section--linha"><table>`;
		
		htmlMatriz += `<th></th>`;	

		// coloca o cabeçalho
		for(var i = 0; i < parseInt(quantidade_nos); i++){//percorreo cada vertice mesmo que não seja adjacente
			htmlMatriz += `<th>`+alfabeto[i]+`</th>`;	
		}

		vetor.forEach(function(value, index) {//percorre cada vertice

			htmlMatriz += `<tr>
			<td>`+alfabeto[index]+`</td>`;
			if(tipo == 'nao')
				for(var i = 0; i < parseInt(quantidade_nos); i++){//percorreo cada vertice mesmo que não seja adjacente
					if(~value.indexOf(i))//se existir dentro do array ele coloca 1 se não ele coloca 0
						htmlMatriz += `<td>1</td>`;
					else
						htmlMatriz += `<td>0</td>`;

				}	
			else
				for(var i = 0; i < parseInt(quantidade_nos); i++){//percorreo cada vertice mesmo que não seja adjacente

					var contido = false, local = 0;

					for(var j = 0; j < value.length; j++){//percorreo cada vertice mesmo que não seja adjacente
					
						if(value[j][1] == i){ // verifica se dentro do vetor existe alguem com o mesmo numero do looping anterior, se existe salva as informações para serem exibidas abaixo
							local = j;
							contido = true;
						}

					}

					if(contido){//se existir dentro do array ele coloca o valor se não ele coloca 0
						htmlMatriz += `<td>`+value[local][0]+`</td>`;
					}
					else
						htmlMatriz += `<td>0</td>`;

				}	

			htmlMatriz += `<tr>`;

		});


		htmlMatriz += `</table></div>`;

		$('.section--2').append(htmlMatriz);
	},
	criarListaComMatriz: function(matriz, quantidade_nos, tipo) {//cria lista e preenche ela com conteúdo da tradução
		var alfabeto = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

		var htmlLista =  `
			<div class="section--linha">
				<ul class="lista">
		`;


		matriz.forEach(function(value, index) {//percorre cada vertice

			var adjacentes = '';
			
			value.forEach(function(_value, _index) {
				if (tipo == 'nao') {
					if (_value == '1') 
						adjacentes += alfabeto[_index]+', ';
				}
				else {
					if (_value != '0') 
						adjacentes += '('+_value+','+alfabeto[_index]+')'+'; ';
				}
				
				
			})

			adjacentes = adjacentes.substring(0, adjacentes.length - 2);
			if (adjacentes == '')
				adjacentes = ' '
			
			htmlLista += `
				<li>
					<label>`+alfabeto[index]+`:</label>
					<input type="text" value="`+adjacentes+`"/>	
				</li>`;
		});
					

		htmlLista += 
				`</ul>
			</div>`;



		$('.section--2').append(htmlLista);
	},
	valida: function(tipo) {
		var _return = true;
		if(tipo == 'todos'){
			$('input').each(function() {
				if(!$(this).val()){
					alert('Preencha todos os campos!');
					_return = false;
					return false;
				}

			})
		}

		return _return;
	}

}



// Execulta tudo que estiver dentro da função ao terminar de carregar o DOM
$(document).ready(function() {
	transformar.init();
})